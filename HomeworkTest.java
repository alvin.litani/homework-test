import java.util.ArrayList;

public class HomeworkTest {
  
  public static void main(String[] args) {
    
    /* Test code for question 1
    int[] nums = new int[4];    
    nums[0] = 3;
    nums[1] = 1;
    nums[2] = 4;   
    nums[3] = 2; 
    int[] answer = QuestionOne(nums);
    
    for (int i=0; i<answer.length; i++)
      System.out.println(answer[i]);
    */
    
    /* Test code for question 2
    int[] nums = new int[4];    
    int x = 4;
    nums[0] = 1;
    nums[1] = 2;
    nums[2] = 3;   
    nums[3] = 4; 
    int[] answer = QuestionTwo(nums,x);
    
    for (int i=0; i<answer.length; i++)
      System.out.println(answer[i]);
    */
    
    /* Test code for question 3
    String word = "souvenir loud four lost"; 
    int x = 4;
    String[] answer = QuestionThree(word,x);
    
    for (int i=0; i<answer.length; i++)
      System.out.println(answer[i]);
    */    
  }
  
  // function for question 1
  public static int[] QuestionOne (int[] integerArray) {
    
    boolean positiveNumber = true;    
    ArrayList<Integer> values = new ArrayList<Integer>();
    
    // check an integer if it always returns > 0 if subtracted by any integers in the array 
    // if it pass the check, add it into the arraylist
    for (int index = 0;index < integerArray.length;index++) {
      for (int n:integerArray) {        
        if(integerArray[index] - n < 0) 
          positiveNumber = false;           
      }
      
      if(positiveNumber)
        values.add(integerArray[index]);      
      
      positiveNumber = true; 
    }
    
    // converts arraylist containing results into array and return that array
    int[] array = new int[values.size()];    
    for (int index = 0;index < array.length;index++) 
      array[index] = values.get(index);
    
    return array;
  }
  
  // function for question 2
  public static int[] QuestionTwo(int[] integerArray, int x) {
    
    boolean returnx = false;    
    ArrayList<Integer> values = new ArrayList<Integer>();
    
    // check an integer if it always returns !x if divided by any integers in the array 
    // if it pass the check, add it into the arraylist
    for (int index = 0;index < integerArray.length;index++) {
      for (int n:integerArray) {        
        if(integerArray[index] / n  == x) 
          returnx = true;           
      }
      
      if(!returnx)
        values.add(integerArray[index]);      
      
      returnx = false; 
    }
    
     // converts arraylist containing results into array and return that array
    int[] array = new int[values.size()];    
    for (int index = 0;index < array.length;index++) 
      array[index] = values.get(index);
    
    return array;    
  }
  
  // function for question 3
  public static String[] QuestionThree(String str, int x) {
    
    ArrayList<String> values = new ArrayList<String>();
    
    // Split the long string into separate words assuming the delimiter is only whitespace
    String[] words = str.split(" ");
    
    // check a word if its length is x
    // if it pass the check, add it into the arraylist
    for(String s:words) {
      if(s.length() == x)
        values.add(s);
    }
    
    // converts arraylist containing results into array and return that array
    String[] array = new String[values.size()];    
    for (int index = 0;index < array.length;index++) 
      array[index] = values.get(index);
    
    return array;   
  }
}




